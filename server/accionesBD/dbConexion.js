function connection(callback){

  var mysql = require('mysql');
  //var shortId 		= require('shortid');
  var connection = mysql.createConnection({
     host: 'localhost',
     user: 'root',
     password: '',
     database: 'roots_project',
     port: 3306
  });
  return callback (connection);
}

module.exports.connection = connection;

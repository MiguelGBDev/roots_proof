function accesoRegistro(paquete, callback){
  var mysql = require('mysql');

  var nombre = paquete.alias;
  var pass = paquete.password1;

  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){
      var consultaSelectUser = 'SELECT id_user, password_user FROM users where alias_user = ?';
      connection.query( consultaSelectUser, [nombre], function(error, resultado){
        if(error){
          throw error;
          return callback (0);
        }else{
          if(resultado.length > 0){
             return callback (1);
          }else{

            var consultaInsertUser = 'INSERT INTO users (alias_user, password_user) values(?, ?)';
            connection.query(consultaInsertUser, [nombre, pass], function(error, result){
              if(error){
                throw error;
                return callback (0);
              }else{
                console.log("Se ha anadido correctamente a "+nombre);
                return callback (2);
              }
            });
          }
        }
      });
    }else
        console.log("fallo en bd");
  });
}

module.exports.accesoRegistro = accesoRegistro;

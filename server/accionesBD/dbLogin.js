function userLogin(paquete, callback){

var alias = paquete.alias;
var pass = paquete.password;

//console.log(alias);
//console.log(pass);

  var mysql = require('mysql');
  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){
      var consultaSelectUser = 'SELECT * FROM users where alias_user = ?';
      connection.query(consultaSelectUser, [alias], function(error, result_select_user){
        if(error){
          return callback (0);  //error
          throw error;
        }else{
          if(result_select_user.length > 0){
            //console.log("IMPRIMO");
            //console.log(result_select_user[0].password_user);
            if(result_select_user[0].password_user == pass)
              return callback (3, result_select_user[0]);    //conexion correcta
            else
              return callback (2);    //contraseña incorrecta
          }else
            return callback (1);      //no existe usuario
        }
      });
    }else
      console.log("fallo en conexion");
  });
}

module.exports.userLogin = userLogin;

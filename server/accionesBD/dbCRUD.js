function new_note(id_user, text_note, callback){

  var mysql = require('mysql');
  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){
      var insertNewNote = 'INSERT INTO notes(id_user_note, text_note) VALUES (?, ?)';
      connection.query(insertNewNote, [id_user, text_note], function(error, result_new_note){
        if(error){
          return callback (0);  //error
          throw error;
        }else{          
          return callback (1);      //nota insertada correctamente
        }
      });
    }else
      console.log("fallo en conexion");
  });
}

function show_notes(id_user, callback){

  var mysql = require('mysql');
  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){
      var consultNotes = 'SELECT * FROM notes where id_user_note = ?';
      connection.query(consultNotes, [id_user], function(error, result_show_notes){
        if(error){
          return callback (0);  //error
          throw error;
        }else{
          if(result_show_notes.length > 0)                  
            return callback (2, result_show_notes);    //recuperacion notas correcta
          else
            return callback (1);      //no existen notas para el usuario
        }
      });
    }else
      console.log("fallo en conexion");
  });
}

function show_one_note(id_user, note_in_order, callback){

  var mysql = require('mysql');
  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){
      var consultOneNote = 'SELECT * FROM notes where id_user_note = ?';
      connection.query(consultOneNote, [id_user], function(error, result_show_one_note){
        if(error){
          return callback (0);  //error
          throw error;
        }else{
          if(result_show_one_note.length < note_in_order)                            
            return callback (2);      //el usuario tiene menos notas de la que se solicita
          else if(result_show_one_note.length > 0)
            return callback (3, result_show_one_note[note_in_order-1]);    //recuperacion notas correcta                      
          else 
            return callback (1);      //el usuario no tiene ninguna nota
        }
      });
    }else
      console.log("fallo en conexion");
  });
}

function delete_note(id_note, callback){

  var mysql = require('mysql');
  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){      
      var deleteNote = 'DELETE FROM notes WHERE id_note = ?';
      connection.query(deleteNote, [id_note], function(error, result_delete_note){
        if(error){
          return callback (0);  //error
          throw error;
        }else{          
          return callback (1);      //Nota borrada correctamente
        }
      });
    }else
      console.log("fallo en conexion");
  });
}

function fav_note(id_note, favOrNot, callback){

  var mysql = require('mysql');
  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){
      var setFavNote = 'UPDATE notes SET fav_note = ? WHERE id_note = ?';      
      connection.query(setFavNote, [favOrNot, id_note], function(error, result_update_fav){
        if(error){
          return callback (0);  //error
          throw error;
        }else{
          return callback (1);      //actualizacion fav correcta
        }
      });
    }else
      console.log("fallo en conexion");
  });
}

function show_favs(id_user, callback){

  var mysql = require('mysql');
  var sconnection = require('./dbConexion.js');
  sconnection.connection(function(connection){
    if(connection){
      var showFavs = 'SELECT * FROM notes WHERE id_user_note = ? AND fav_note = 1';
      connection.query(showFavs, [id_user], function(error, result_show_favs){
        if(error){
          return callback (0);  //error
          throw error;
        }else{
          if(result_show_favs.length > 0)                  
            return callback (2, result_show_favs);    //recuperacion notas favoritas
          else
            return callback (1);      //no existen notas para el usuario
        }
      });
    }else
      console.log("fallo en conexion");
  });
}


module.exports.new_note = new_note;
module.exports.show_notes = show_notes;
module.exports.show_one_note = show_one_note;
module.exports.delete_note = delete_note;
module.exports.fav_note = fav_note;
module.exports.show_favs = show_favs;
var express = require('express');
var app = express();
var server = require('http').Server(app);

//var util = require('util');
var querystring = require('querystring')
var path = require('path');

app.use(express.static('../public'));
var registro = require('./accionesBD/dbRegistro.js');
var login = require('./accionesBD/dbLogin.js');
//var notesUser = require('./accionesBD/dbSendInfo.js');
var crud = require('./accionesBD/dbCRUD.js');

var user = {};


app.get("/userLogin", function(req, res) {
     res.send(user);
});

app.post('/registro', function(req, res) {

  var dataString = '';
  req.on('data', function(data){
      dataString += data;
    });
  req.on('end', function(){
    var dataObject =  querystring.parse(dataString);

    registro.accesoRegistro(dataObject, function(retorno){
      if(retorno){
        if(retorno == 2)
          res.sendFile(path.resolve('../public/registroCorrecto.html'));
        else if(retorno == 1)
          res.sendFile(path.resolve('../public/nombreOcupado.html'));
        else
          res.sendFile(path.resolve('../public/error.html'));
      }
    });
  });
});

app.post('/acceso', function(req, res) {

  var dataString = '';
  req.on('data', function(data){
    dataString += data;
  });
  req.on('end', function(){
    var dataObject =  querystring.parse(dataString);

    login.userLogin(dataObject, function(correcto, userData){
      switch(correcto){
        case 0:
          console.log("fallo en retorno");
          res.sendFile(path.resolve('../public/error.html'));
        break;
        case 1:
          console.log("no existe usuario");
          res.sendFile(path.resolve('../public/noUser.html'));
        break;
        case 2:
          console.log("password incorrecto");
          res.sendFile(path.resolve('../public/wrongPass.html'));
        break;
        case 3:
          console.log("password Correcto");
          res.sendFile(path.resolve('../public/menuUser.html')); 
          user = userData;                    
        break;
      }
    });
  });
});

app.post('/nuevaNota', function(req, res) {

  var dataString = '';
  req.on('data', function(data){      
      dataString += data;
  });
  req.on('end', function(){
    var dataObject =  querystring.parse(dataString);
    crud.new_note(user.id_user, dataObject.texto, function(correcto){
      if(correcto){
        if(correcto == 0)
          console.log("Error de insercion");
        else
          console.log("Nueva nota insertada correctamente");
      }
    });  
  });
});

app.get('/todasNotas', function(req, res) {

  crud.show_notes(user.id_user, function(correcto, notes){
    switch(correcto){
      case 0:
        console.log("error en retorno");
        res.sendFile(path.resolve('../public/error.html'));
      break;
      case 1:
        console.log("no existe ninguna nota para este usuario");
        //res.sendFile(path.resolve('../public/noUser.html'));
      break;
      case 2:          
        var textos = [];
        for(var i = 0; i < notes.length; i++)
          textos.push(notes[i].text_note);
        
        res.send(textos);
      break;        
    }  
  });  
});

app.post('/unaNotaDet', function(req, res) {

  var dataString = '';
  req.on('data', function(data){      
      dataString += data;
  });
  req.on('end', function(){
    var dataObject =  querystring.parse(dataString);
    console.log(dataObject);
    crud.show_one_note(user.id_user, dataObject.numeroNota, function(correcto, nota){      
      switch(correcto){
        case 0:
          var nota = { text_note : "No existe ninguna nota para este usuario" };
          console.log("error en retorno");          
        break;
        case 1:
          var nota = { text_note : "No existe ninguna nota para este usuario" };
          console.log("No existe ninguna nota para este usuario");          
          //res.send(nota);
        break;
        case 2:
          console.log("El usuario no posee tantas notas como el numero solicitado");          
          var nota = { text_note : "El usuario no posee tantas notas como el numero solicitado" };
          //res.send(nota);
        break;
        case 3:        
          console.log(nota);                      
          //res.send(nota);
        break;        
      }
      res.send(nota);
    });      
  });
});

app.post('/nuevoFav', function(req, res){

  var dataString = '';
  req.on('data', function(data){      
      dataString += data;
  });
  req.on('end', function(){
    var dataObject =  querystring.parse(dataString);    

    crud.show_notes(user.id_user, function(correcto, notes){
      switch(correcto){
        case 0:
          console.log("error en retorno");        
        break;
        case 1:
          console.log("no existe ninguna nota para este usuario");        
        break;
        case 2:         
          var elegida = notes[dataObject.favNumber-1];        
          var contraria;
          if(elegida.fav_note == 1)
            contraria = 0;
          else
            contraria = 1;

          crud.fav_note(elegida.id_note, contraria, function(correcto){    
            if(correcto){
              var mensaje;
              if(correcto == 0)                
                mensaje = "Error al marcar favorito";
              else{                
                if(contraria == 0)
                  mensaje = "Has desmarcado un favorito";  
                else
                  mensaje = "Favorito marcado correctamente";
              }
              res.send(mensaje);
            }
          });  
        break;        
      }  
    });  
  });
});

app.get('/todasFavs', function(req, res) {

  crud.show_favs(user.id_user, function(correcto, notes){
    switch(correcto){
      case 0:
        console.log("error en retorno");
        //res.sendFile(path.resolve('../public/error.html'));
      break;
      case 1:
        console.log("no existe ninguna nota para este usuario");
        //res.sendFile(path.resolve('../public/noUser.html'));
      break;
      case 2:          
        var favoritos = [];
        for(var i = 0; i < notes.length; i++)
          favoritos.push(notes[i].text_note);
        
        res.send(favoritos);
      break;        
    }  
  });  
});

app.post('/borrarNota', function(req, res) {

  var dataString = '';
  req.on('data', function(data){      
      dataString += data;
  });
  req.on('end', function(){
    var dataObject =  querystring.parse(dataString);
    console.log(dataObject);
    crud.show_one_note(user.id_user, dataObject.numeroNotaBorrar, function(correcto, nota){      
      switch(correcto){
        case 0:         
          console.log("Error en retorno");          
        break;
        case 1:          
          console.log("No existe ninguna nota para este usuario");          
          //res.send(nota);
        break;
        case 2:          
          console.log("El usuario no posee tantas notas como el numero solicitado");                    
          //res.send(nota);
        break;
        case 3:     
          crud.delete_note(nota.id_note, function(correcto1){             
            if(correcto1 == 0)              
              console.log("Error en el proceso");                            
            else
              console.log("La nota: "+nota.text_note+" ha sido borrada");                                         
          });
        break;        
      }      
    });      
  });
});

server.listen(8080, function(){
  console.log("servidor corriendo en 8080")
});

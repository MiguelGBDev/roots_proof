

 var app = angular.module('myApp', []);

    // configure our routes
    app.config(['$routeProvider', function($routeProvider) {
        $routeProvider

        .when('/newNote', {
            templateUrl : 'newNote.html',
            controller  : 'newNoteCtrl'
        })
        .when('/allNotes', {
            templateUrl : 'allNotes.html',
            controller  : 'allNotesCtrl'        
        })
        .when('/oneNoteDet', {
            templateUrl : 'oneNoteDet.html',
            controller  : 'oneNoteCtrl'        
        })
        .when('/newFav', {
            templateUrl : 'newFav.html',
            controller  : 'newFavCtrl'        
        })
        .when('/allFavs', {
            templateUrl : 'allFavs.html',
            controller  : 'allFavsCtrl'
        })
        .when('/deleteNote', {
            templateUrl : 'deleteNote.html',
            controller  : 'deleteNoteCtrl'
        });
    }]);

    app.controller('mainController', function($scope, $http) {

        $http({
            method: 'GET',
            url: 'http://localhost:8080/userLogin'
            }).then(function mySuccess(response) {
               $scope.dataUser = response.data;
            }, function myError(response) {                
        });
    });

    app.controller('allNotesCtrl', function($scope, $http) {

        $http({
            method: 'GET',
            url: 'http://localhost:8080/todasNotas'
            }).then(function mySuccess(response) {
                //console.log(response.data);
               $scope.arrayNotes = response.data;
            }, function myError(response) {            
        });
    });

    app.controller('newNoteCtrl', function($scope,$http){

        $scope.crearNota = function(data){

            console.log(data.texto);
            var data = $.param({
                texto: data.texto
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post("http://localhost:8080/nuevaNota", data, config)
            .success(function (data, status, headers, config){              
              $scope = data.texto;
            })
            .error(function (data, status, headers, config){
              $scope[error] = "SUBMIT ERROR";
            });
        }      
    });

    app.controller('oneNoteCtrl', function($scope,$http){

        $scope.mostrarNota = function(data){

            console.log(data.notaNumero);
            var data = $.param({
                numeroNota: data.notaNumero
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post("http://localhost:8080/unaNotaDet", data, config)
            .success(function (data, status, headers, config){
              console.log(data);                
              $scope.notaText = data.text_note;
              console.log($scope.notaText);
            })
            .error(function (data, status, headers, config){
              $scope[error] = "SUBMIT ERROR";
            });
        }      
    });

    app.controller('newFavCtrl', function($scope,$http){
 
        $scope.marcarFav = function(data){

            console.log(data.favNumber);
            var data = $.param({
                favNumber: data.favNumber
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post("http://localhost:8080/nuevoFav", data, config)
            .success(function (data, status, headers, config){
              console.log(data);                
              //$scope.mensaje = data.mensaje;
              $scope.mensaje = data;
              console.log($scope.mensaje);
            })
            .error(function (data, status, headers, config){
              $scope[error] = "SUBMIT ERROR";
            });
        }      
    });

    app.controller('allFavsCtrl', function($scope, $http) {

        $http({
            method: 'GET',
            url: 'http://localhost:8080/todasFavs'
            }).then(function mySuccess(response) {
                //console.log(response.data);
               $scope.arrayFavs = response.data;
            }, function myError(response) {            
        });
    });

    app.controller('deleteNoteCtrl', function($scope,$http){

        $scope.borrarNota = function(data){

            console.log(data.notaNumeroBorrar);
            var data = $.param({
                numeroNotaBorrar: data.notaNumeroBorrar
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post("http://localhost:8080/borrarNota", data, config)
            .success(function (data, status, headers, config){
              console.log(data);                
              $scope.text_deleted = data.text_deleted;
              console.log($scope.text_deleted);
            })
            .error(function (data, status, headers, config){
              $scope[error] = "SUBMIT ERROR";
            });
        }      
    });